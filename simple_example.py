import numpy as np
import convInterpolate as CI
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation, PillowWriter

# LOAD FRAMES
frames = np.load("resources/simple_example_frames.npy")
frameCount = frames.shape[2]
frameHeight = frames.shape[1]
frameWidth = frames.shape[0]

# CONVERT
converter = CI.convInterpolate(0.2, 100, 0.1, frames[:, :, 0], log_input=True)
EVENT_LIST = []
ACTIVATION_LIST = []

for n in range(1, frameCount):
    new_events, event_frame = converter.update(frames[:, :, n])
    EVENT_LIST = EVENT_LIST + new_events
    ACTIVATION_LIST.append(event_frame)
    print(f'Converted: {n}/{frameCount-1}')

event_data = np.array(EVENT_LIST, int)
np.savetxt("events.csv", event_data, delimiter=",", fmt='%d, %d, %d, %d')

# PROMPT TO PROCEED
_ = input("Converted and saved. Press any key to continue...")


# PLAY VIDEO using Matplotlib
fig = plt.figure(figsize=(10, 4.5))
ax_frames = fig.add_subplot(1, 2, 1)
ax_events = fig.add_subplot(1, 2, 2)

fig.subplots_adjust(bottom=0.1, top=0.9, hspace=0.24, right=0.95, left=0.07)


def animate(i):

    im = [ax_frames.imshow(frames[:, :, i], cmap='gray'),
          ax_events.imshow(ACTIVATION_LIST[i], cmap='seismic', vmax=1, vmin=0)]
    return im


def init():
    im = []

    ax_frames.set_title("Conventional camera view", fontweight='bold', fontsize=15)
    ax_frames.set_xticks(np.arange(0, 129, 16))
    ax_frames.set_yticks(np.arange(0, 129, 16))
    ax_frames.xaxis.set_tick_params(labelsize=15)
    ax_frames.yaxis.set_tick_params(labelsize=15)
    ax_frames.set_xlim([0, frameWidth])
    ax_frames.set_ylim([0, frameHeight])

    ax_events.set_title("Event camera view", fontweight='bold', fontsize=15)
    ax_events.set_xticks(np.arange(0, 129, 16))
    ax_events.set_yticks(np.arange(0, 129, 16))
    ax_events.xaxis.set_tick_params(labelsize=15)
    ax_events.yaxis.set_tick_params(labelsize=15)
    ax_events.set_xlim([0, frameWidth])
    ax_events.set_ylim([0, frameHeight])

    im.append(ax_frames.imshow(frames[:, :, 0].T, cmap='gray'))
    im.append(ax_events.imshow(np.zeros([frameWidth, frameHeight]), cmap='seismic', ))

    return im


ani = FuncAnimation(fig, animate, interval=10, blit=True, frames=frameCount-1, init_func=init)

plt.show()
